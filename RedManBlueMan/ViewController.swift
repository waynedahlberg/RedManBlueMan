//
//  ViewController.swift
//  RedManBlueMan
//
//  Created by Wayne Dahlberg on 9/5/15.
//  Copyright © 2015 Wayne Dahlberg. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    @IBOutlet weak var redManImage: UIImageView!
    @IBOutlet weak var blueManImage: UIImageView!
    @IBOutlet weak var redHideButton: UIButton!
    @IBOutlet weak var blueHideButton: UIButton!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    @IBAction func hideBlueMan(sender: AnyObject) {
        blueManImage.hidden = true
        
    }

    @IBAction func hideRedMan(sender: AnyObject) {
        redManImage.hidden = true
    }

}

